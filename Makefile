CC = gcc
CPPFLAGS = -D_DEFAULT_SOURCE
CFLAGS = -g -Wall -Wextra -Werror -std=c99 -fPIC -fvisibility=hidden -fno-builtin
LDFLAGS = -shared -lpthread
VPATH = src

SRC = malloc.c bucket.c bucket_man.c page_man.c bucket_list.c big_alloc.c
TESTS=test_bucket test_page_man test_bucket_list

TARGET_LIB = libmalloc.so
OBJS = ${SRC:.c=.o}
CALL_LIB = libtracemalloc.so
CALL_OBJS = call_trace.o

all: $(TARGET_LIB)

$(TARGET_LIB): CFLAGS += -pedantic
$(TARGET_LIB): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^

debug: CFLAGS += -g
debug: clean $(TARGET_LIB)

trace: $(CALL_LIB)

check: $(TESTS)
	for t in $(TESTS); do \
		./$$t; \
	done; \
	tests/test_suite.sh

test_%: tests/test_%.o src/%.o
	$(CC) $(CFLAGS) -lcriterion -o $@ $^

test_bucket_list: tests/test_bucket_list.o src/bucket_list.o src/bucket.o
	$(CC) $(CFLAGS) -lcriterion -o $@ $^

$(CALL_LIB): CPPFLAGS = -D_GNU_SOURCE
$(CALL_LIB): CFLAGS += -g
$(CALL_LIB): LDFLAGS += -ldl
$(CALL_LIB): $(CALL_OBJS)
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	$(RM) $(TARGET_LIB) $(CALL_LIB) $(OBJS) $(TESTS) $(CALL_OBJS)

.PHONY: all $(TARGET_LIB) $(CALL_LIB) trace clean

