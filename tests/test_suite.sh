#!/bin/sh

set -e

echo "Testing malloc..."
LD_PRELOAD=./libmalloc.so factor 20 30 40 50 60 70 80 90 > /dev/null
LD_PRELOAD=./libmalloc.so cat Makefile > /dev/null
LD_PRELOAD=./libmalloc.so ip a > /dev/null
echo "Malloc OK"

echo "Testing realloc"
LD_PRELOAD=./libmalloc.so ls > /dev/null
LD_PRELOAD=./libmalloc.so ls -la > /dev/null
echo "Realloc OK"

# LD_PRELOAD=./libmalloc.so less Makefile

