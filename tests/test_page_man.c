#include <criterion/criterion.h>
#include "../src/page_man.h"


Test(Page_man, Alloc_free)
{
    void *ptr = alloc_page();
    free_page(ptr);
}


Test(Page_man, Several_alloc_free)
{
    void *ptr1 = alloc_page();
    void *ptr2 = alloc_page();

    free_page(ptr2);
    free_page(ptr1);
}

