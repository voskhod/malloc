#include <criterion/criterion.h>
#include "../src/bucket.h"


struct test
{
    size_t a;
    size_t b;
};

size_t get_page_size()
{
    return (sizeof(struct bucket) + sizeof(struct test) * 2);
}

struct bucket *b = NULL;

void *alloc_page()
{
    return malloc(get_page_size());
}

void free_page(void *ptr)
{
    free(ptr);
}

struct bucket *get_metadata(void *ptr)
{
    ptr = ptr;
    return b;
}


Test(Bucket, Init_and_destroy)
{
    b = bucket_init(sizeof(struct test));
    cr_assert(b);
    bucket_destroy(b);
}


Test(Bucket, Several_alloc)
{
    b = bucket_init(sizeof(struct test));

    struct test *x = bucket_allocate(b);
    x->a = 1;

    struct test *y = bucket_allocate(b);
    y->a = 1;

    bucket_destroy(b);
}

Test(Bucket, Bucket_full)
{
    b = bucket_init(sizeof(struct test));

    struct test *x = bucket_allocate(b);
    x->a = 1;

    struct test *y = bucket_allocate(b);
    y->a = 1;

    struct test *z = bucket_allocate(b);
    cr_assert_eq(z, NULL);

    bucket_destroy(b);
}


Test(Bucket, Free)
{
    b = bucket_init(sizeof(struct test));

    struct test *x = bucket_allocate(b);
    x->a = 1;
    cr_assert_eq(b->nb_blocks, 1);
    
    struct test *y = bucket_allocate(b);
    y->a = 1;
    cr_assert_eq(b->nb_blocks, 2);

    bucket_free(x);
    cr_assert_eq(b->nb_blocks, 1);
    bucket_free(y);
    cr_assert_eq(b->nb_blocks, 0);

    x = bucket_allocate(b);
    x->a = 1;
    y = bucket_allocate(b);
    y->a = 1;

    bucket_destroy(b);
}

