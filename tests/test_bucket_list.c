#include <criterion/criterion.h>
#include <err.h>
#include "../src/bucket_list.h"
#include "../src/bucket.h"


size_t get_page_size()
{
    return 4096;
}

void *alloc_page()
{
    return malloc(get_page_size());
}

void free_page(void *ptr)
{
    ptr = ptr;
}

struct bucket *get_metadata(void *ptr)
{
    errx(1, "Get_metadata");
    return ptr;
}

Test(Bucket_list, Insert_one)
{
    struct bucket *head = bucket_init(1024);
    head->nb_blocks = 1;
    struct bucket *new = bucket_init(1024);
    new->nb_blocks = 2;

    head = push_front(head, new);

    cr_assert_eq(head, new);
    cr_assert_eq(head->nb_blocks, 2);
}


Test(Bucket_list, Insert_many)
{
    struct bucket *head = bucket_init(1024);
    head->nb_blocks = 1;
 
    struct bucket *new1 = bucket_init(1024);
    new1->nb_blocks = 2;

    struct bucket *new2 = bucket_init(1024);
    new2->nb_blocks = 3;

    head = push_front(head, new1);
    cr_assert_eq(head->nb_blocks, 2);

    head = push_front(head, new2);
    cr_assert_eq(head->nb_blocks, 3);
}


Test(Bucket_list, Delete)
{
    struct bucket *head = bucket_init(1024);
    head->nb_blocks = 1;
 
    struct bucket *new1 = bucket_init(1024);
    new1->nb_blocks = 2;

    struct bucket *new2 = bucket_init(1024);
    new2->nb_blocks = 3;

    head = push_front(head, new1);
    cr_assert_eq(head->nb_blocks, 2);

    head = push_front(head, new2);
    cr_assert_eq(head->nb_blocks, 3);

    head = delete(head, new1);
    head = delete(head, head);
    head = delete(head, new2);
}

