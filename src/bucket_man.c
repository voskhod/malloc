#include <err.h>
#include <stddef.h>

#include "bucket_man.h"
#include "bucket_list.h"


enum bucket_type
{
    BUCK_32 = 0,
    BUCK_64,
    BUCK_128,
    BUCK_256,
    BUCK_512,
    BUCK_1024,
    BUCK_BIG
};


static struct bucket *b_man[6] = { NULL };

static int bucket_try_allocate(struct bucket *bucket, void **ptr)
{
    for(; bucket != NULL; bucket = bucket->next)
    {
        *ptr = bucket_allocate(bucket);

        if (*ptr != NULL)
            return 1;
    }

    return 0;
}

static void *bucket_allocate_and_init(enum bucket_type b_type, size_t size)
{
    void *ptr = NULL;
    if(!bucket_try_allocate(b_man[b_type], &ptr))
    {
        struct bucket *new = bucket_init(size);
        if (new == NULL)
            return NULL;

        b_man[b_type] = push_front(b_man[b_type], new);
        ptr = bucket_allocate(b_man[b_type]);
    }

    return ptr;
}


static size_t get_size(size_t size)
{
    if (size <= 32)
        size = 32;

    else if (size <= 64)
        size = 64;

    else if (size <= 128)
        size = 128;

    else if (size <= 256)
        size = 256;

    else if (size <= 512)
        size = 512;

    else if (size <= 1024)
        size = 1024;

    return size;
}


enum bucket_type get_bucket_type(size_t size)
{
    if (size <= 32)
        return BUCK_32;

    else if (size <= 64)
        return BUCK_64;

    else if (size <= 128)
        return BUCK_128;

    else if (size <= 256)
        return BUCK_256;

    else if (size <= 512)
        return BUCK_512;

    else if (size <= 1024)
        return BUCK_1024;

    return BUCK_BIG;
}



void *b_man_allocate(size_t size)
{
    size = get_size(size);
    enum bucket_type type = get_bucket_type(size);

    void *ptr = bucket_allocate_and_init(type, size);

    return ptr;
}


void b_man_free(void *ptr)
{
    struct bucket *bucket = get_metadata(ptr);
    if (bucket == NULL)
        return;


    if(bucket_free(ptr))
        return;

    if (bucket->nb_blocks == 0)
    {
        enum bucket_type b_type = get_bucket_type(bucket->block_size);

        if (b_type != BUCK_BIG)
            b_man[b_type] = delete(b_man[b_type], bucket);

        bucket_destroy(bucket);
    }
}
