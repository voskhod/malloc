#ifndef BUCKET_MAN_H
#define BUCKET_MAN_H

#include "bucket.h"

void *b_man_allocate(size_t size);
void b_man_free(void *ptr);

#endif /* BUCKET_MAN_H */
