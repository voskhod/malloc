#ifndef BUCKET_LIST
#define BUCKET_LIST

#include "bucket.h"

struct bucket *push_front(struct bucket *head, struct bucket *new);
struct bucket *delete(struct bucket *head, struct bucket *elm);


#endif /* BUCKET_LIST  */
