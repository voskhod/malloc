#ifndef BUCKET_H
#define BUCKET_H

#include <stddef.h>
#include "page_man.h"

#define BUCK_DATA_OFFSET sizeof(struct bucket)
#define BUCK_DATA_SIZE (get_page_size() - BUCK_DATA_OFFSET)


struct bucket
{
    struct bucket *next;
    struct bucket *prev;

    size_t block_size;
    size_t nb_blocks;
    void *free_blocks;

    // Uselless field, just to have 
    // sizeof(struct bucket) align on 16
    int _align;
};

struct bucket *bucket_init(size_t block_size);
void bucket_destroy(struct bucket *bucket);

void *bucket_allocate(struct bucket *bucket);
int bucket_free(void *ptr);

#endif /* BUCKET_H */
