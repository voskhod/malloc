#include <stddef.h>
#include <sys/mman.h>

#include "page_man.h"
#include "big_alloc.h"

void *calloc(size_t nmemb, size_t size);
void free(void *ptr);

static struct ref *list = NULL;

struct ref
{
    struct ref *next;
    struct ref *prev;

    void *ptr;
    size_t size;
};


static struct ref *ref_init(void *ptr, size_t size)
{
    struct ref *ref = calloc(1, sizeof(struct ref));
    if (ref == NULL)
        return NULL;

    ref->ptr = ptr;
    ref->size = size;

    return ref;
}


static int ref_add(void *ptr, size_t size)
{
    struct ref *new = ref_init(ptr, size);
    if (new == NULL)
        return 0;

    if (list != NULL)
    {
        new->next = list;
        list->prev = new;
    }

    list = new;

    return 1;
}

static size_t page_align(size_t size)
{
    size_t page_size = get_page_size();
    return (size - 1) / page_size * page_size + page_size;
}

void *big_alloc(size_t size)
{
    size = page_align(size);

    void *ptr = mmap(NULL, size,
                        PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    if (ptr == MAP_FAILED)
        return NULL;

    if (!ref_add(ptr, size))
        return NULL;

    return ptr;
}


void big_free(void *ptr, size_t size)
{

    struct ref *to_delete = list;
    for (; to_delete != NULL; to_delete = to_delete->next)
    {
        if (to_delete->ptr == ptr)
            break;
    }

    if (to_delete->next != NULL)
        to_delete->next->prev = to_delete->prev;

    if (to_delete->prev != NULL)
        to_delete->prev->next = to_delete->next;
    else
        list = to_delete->next;

    free(to_delete);

    munmap(ptr, size);
}


int is_big_alloc_size(void *ptr)
{
    for (struct ref *cur = list; cur != NULL; cur = cur->next)
    {
        if (cur->ptr == ptr)
            return cur->size;
    }

    return 0;
}
