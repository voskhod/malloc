#include <stddef.h>
#include <string.h>

#include "big_alloc.h"
#include "bucket_man.h"

#include <err.h>

__attribute__((visibility("default")))
void *malloc(size_t size)
{
    void *ptr = NULL;
    if (size <= 1024)
        ptr = b_man_allocate(size);
    else
        ptr = big_alloc(size);

    return ptr;
}


static size_t get_size(void *ptr)
{
    size_t size = is_big_alloc_size(ptr);

    if (size == 0)
    {
        struct bucket *b = get_metadata(ptr);
        size = b->block_size;
    }

    return size;
}

__attribute__((visibility("default")))
void free(void *ptr)
{
    if (ptr == NULL)
        return;

    size_t size = get_size(ptr);
    if (size > 1024)
        big_free(ptr, size);
    else
        b_man_free(ptr);
}


__attribute__((visibility("default")))
void *realloc(void *ptr, size_t size)
{
    if (ptr == NULL)
        return malloc(size);

    if (size == 0)
    {
        free(ptr);
        return NULL;
    }

    size_t old_size = get_size(ptr);

    if(old_size >= size && old_size <= size * 10)
        return ptr;

    void *new = malloc(size);
    if (new == NULL)
        return NULL;

    new = memcpy(new, ptr, old_size);

    free(ptr);

    return new;
}

__attribute__((visibility("default")))
void *calloc(size_t nmemb, size_t size)
{
    if (__builtin_umull_overflow(nmemb, size, &size))
        return NULL;

    void *new = malloc(size);
    if (new == NULL)
        return NULL;

    new = memset(new, 0, size);

    return new;
}
