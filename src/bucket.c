#include "page_man.h"
#include "bucket.h"

static void free_list_init(void *free_list, size_t block_size)
{
    size_t nb_block = BUCK_DATA_SIZE / block_size;
    void **cur = free_list;

    for (size_t i = 0; i < nb_block - 1; i++)
    {
        *cur = cur + (block_size / sizeof(void **));
        cur = *cur;
    }
    *cur = NULL;
}


struct bucket *bucket_init(size_t block_size)
{
    struct bucket *bucket = alloc_page();
    if (bucket == NULL)
        return NULL;

    bucket->next = NULL;
    bucket->prev = NULL;

    bucket->block_size = block_size;
    bucket->nb_blocks = 0;
    bucket->free_blocks = bucket + 1;

    free_list_init(bucket->free_blocks, block_size);

    return bucket;
}


static inline int bucket_is_full(struct bucket *bucket)
{
    return (bucket->free_blocks == NULL);
}

static inline void *get_first_free(struct bucket *bucket)
{
    void *ptr = bucket->free_blocks;
    void **next = bucket->free_blocks;
    bucket->free_blocks = *next;

    return ptr;
}

void *bucket_allocate(struct bucket *bucket)
{
    if (bucket == NULL || bucket_is_full(bucket))
        return NULL;

    bucket->nb_blocks++;

    return get_first_free(bucket);
}


void bucket_destroy(struct bucket *bucket)
{
    free_page(bucket);
}


void add_free(struct bucket *bucket, void *ptr)
{
    void **new_free = ptr;
    *new_free = bucket->free_blocks;
    bucket->free_blocks = new_free;
}


int bucket_free(void *ptr)
{
    if (ptr == NULL)
        return 1;

    struct bucket *bucket = get_metadata(ptr);
    add_free(bucket, ptr);
    bucket->nb_blocks--;

    return 0;
}
