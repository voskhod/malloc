#include "bucket_list.h"

struct bucket *push_front(struct bucket *head, struct bucket *new)
{
    if (head != NULL)
    {
        new->next = head;
        head->prev = new;
    }

    return new;
}


struct bucket *delete(struct bucket *head, struct bucket *elm)
{
    if (elm->next != NULL)
        elm->next->prev = elm->prev;


    if (elm->prev != NULL)
        elm->prev->next = elm->next;
    else
        return elm->next;

    return head;
}

