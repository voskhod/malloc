#include <sys/mman.h>
#include <unistd.h>

#include "page_man.h"

void *alloc_page(void)
{
    void *ptr = mmap(NULL, get_page_size(),
                        PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    if (ptr == MAP_FAILED)
        return NULL;
    return ptr;
}


void free_page(void *page)
{
    munmap(page, get_page_size());
}


inline size_t get_page_size(void)
{
    return sysconf(_SC_PAGESIZE);
}

struct bucket *get_metadata(void *ptr)
{
    return (void*)((size_t)ptr & ~(get_page_size() - 1));
}
