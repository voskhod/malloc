#ifndef PAGE_MAN
#define PAGE_MAN

#include <stddef.h>

void *alloc_page();
void free_page(void *page);

size_t get_page_size();
struct bucket *get_metadata(void *ptr);

#endif /* PAGE_MAN */
