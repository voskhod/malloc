#ifndef BIG_ALLOC_H
#define BIG_ALLOC_H

void *big_alloc(size_t size);
void big_free(void *ptr, size_t size);

int is_big_alloc_size(void *ptr);

#endif /* BIG_ALLOC_H */
