# Malloc
A malloc implementation using a bucket list implementation. The large
allocation are currently slow as they are stored in a linked list. To improve
performance, they should be in a hash table.

# Build
`make` at the root of the project.
